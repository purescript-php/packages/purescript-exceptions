<?php

$exports['showErrorImpl'] = function ($err) {
  return $err->getTraceAsString();
};

$exports['error'] = function ($msg) {
  return new Exception($msg);
};

$exports['message'] = function ($e) {
  return $e->getMesage();
};

$exports['name'] = function ($e) {
  return get_class($e);
};

$exports['stackImpl'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($e) use (&$just, &$nothing) {
      return $e->getTrace() ? just($e->getTrace()) : $nothing;
    };
  };
};

$exports['throwException'] = function ($e) {
  return function () use (&$e) {
    throw $e;
  };
};

$exports['catchException'] = function ($c) {
  return function ($t) use (&$c) {
    return function () use (&$c, &$t) {
      try {
        return $t();
      } catch (Exception $e) {
        return $c($e)();
      }
    };
  };
};
